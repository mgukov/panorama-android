package com.pvrmatic.panoramius.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by mgukov on 09.09.2016.
 */
public final class Metrics {

    private Metrics() {}

    public static float dpToPxRow(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
    public static int dpToPx(float dp) {
        return Math.round(dpToPxRow(dp));
    }

    public static float pxToDp(float px){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static int widthPx() {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return metrics.widthPixels;
    }

    public static int heightPx() {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return metrics.heightPixels;
    }
}
