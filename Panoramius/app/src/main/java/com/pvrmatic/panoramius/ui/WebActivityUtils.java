package com.pvrmatic.panoramius.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;

import com.pvrmatic.panoramius.LocalFileServer;

/**
 * Created by mgukov on 02.10.2016.
 */

public class WebActivityUtils {

    public enum WorkMode {
        Local, Web
    }

    private static final String localPath2d = LocalFileServer.PATH + "3d/2d.html";
    private static final String localPath3d = LocalFileServer.PATH + "3d/3d.html";

    private static final String WebURL = "http://data.varkadyev.tmweb.ru/";

    private static final String webPath2d = WebURL + "p4/2d.html";
    private static final String webPath3d = WebURL + "p4/3d.html";

    private static final WorkMode workMode = WorkMode.Local;


    public static final String ViewModeKey = "ViewModeKey";
    public static final String ViewMode3D = "3D";
    public static final String ViewMode2D = "2D";


    public static Class<?> webActivityClass = WebActivity.class;

    private static String viewMode = ViewMode2D;

    private WebActivityUtils() {
    }

    public static WorkMode getWorkMode() {
        return workMode;
    }

    private static String get2DPath() {
        return workMode == WorkMode.Local ? localPath2d : webPath2d;
    }

    private static String get3DPath() {
        return workMode == WorkMode.Local ? localPath3d : webPath3d;
    }

    public static String getPath() {
        return viewMode.equals(ViewMode2D) ? get2DPath() : get3DPath();
    }

    public static String getDeviceInfo() {
        return Build.BRAND+";"+Build.MODEL+";"+Build.DEVICE+";"+Build.PRODUCT+";"+Build.MANUFACTURER;
    }


    public static void setViewMode(String viewMode) {
        WebActivityUtils.viewMode = viewMode;
    }

    public static void startWebViewActivity(Activity context) {
        Intent startWebIntent = new Intent(context, webActivityClass);
        startWebIntent.putExtra(ViewModeKey, viewMode);
        context.startActivity(startWebIntent);
    }
}
