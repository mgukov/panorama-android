package com.pvrmatic.panoramius.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pvrmatic.panoramius.R;

/**
 * Created by mgukov on 09.09.2016.
 */
public class HelpPageFragment extends Fragment {

    public static final String INDEX_KEY = "PAGE_INDEX";

    private ViewGroup view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {
            view = (ViewGroup) inflater.inflate(R.layout.help_page_fragment, container, false);
            final int index = getArguments().getInt(INDEX_KEY);
            view.setBackgroundResource(getResourceId(index));
        }

        return view;
    }

    private int getResourceId(int index) {
        switch (index) {
            case 0: return R.drawable.page1;
            case 1: return R.drawable.page2;
            case 2: return R.drawable.page3;
            case 3: return R.drawable.page4;
            case 4: return R.drawable.page5;
            case 5: return R.drawable.page6;
        }
        return -1;
    }
}
