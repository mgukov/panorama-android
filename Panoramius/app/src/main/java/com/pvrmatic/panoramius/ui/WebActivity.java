package com.pvrmatic.panoramius.ui;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pvrmatic.panoramius.LocalFileServer;
import com.pvrmatic.panoramius.R;
import com.pvrmatic.panoramius.utils.Trace;

import java.io.IOException;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public final class WebActivity extends AppCompatActivity {

    private LocalFileServer fileServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        final WebView webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setUserAgentString(WebActivityUtils.getDeviceInfo());
        Trace.debug("BuildInfo: "+WebActivityUtils.getDeviceInfo());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new CustomWebViewClient(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (WebActivityUtils.getWorkMode() == WebActivityUtils.WorkMode.Local) {
            try {
                Trace.debug("Start local server");
                fileServer = new LocalFileServer(this);
                fileServer.start();
                Trace.debug("Server started");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final String path = WebActivityUtils.getPath();
        Trace.debug("Load: " + path);

        final WebView webView = (WebView) findViewById(R.id.webview);
        webView.post(new Runnable() {
            @Override
            public void run() {
                try {
                    webView.loadUrl(path);
                } catch (Exception e) {
                    Trace.debug(e.toString());
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (fileServer != null) {
            fileServer.stop();
        }
    }
}

final class CustomWebViewClient extends WebViewClient {

    private static final String stopUrl = "exit://";
    private final WebActivity activity;

    public CustomWebViewClient(WebActivity activity) {
        this.activity = activity;
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        Trace.debug("shouldInterceptRequest: " + url);
        if (url.equals(stopUrl)) {
            activity.finish();
        }
        return super.shouldInterceptRequest(view, url);
    }
}
